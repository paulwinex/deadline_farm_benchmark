from PySide.QtCore import *
import sys, os, getpass, json
from dateutil.parser import parse
import argparse
import Deadline.DeadlineConnect as Connect


class DeadlineBenchmark(QObject):
    completeSignal = Signal()
    updateSignal = Signal()

    job_prefix = 'Benchmark_'

    def __init__(self,
                 scene_file,
                 output_file,
                 frame_count=5,
                 main_slave=None,
                 chain=False):
        super(DeadlineBenchmark, self).__init__()
        self.scene_file = scene_file
        self.db_file = output_file
        self.frame_count = max([frame_count, 5])
        self.main_slave = main_slave
        self.chain = chain

        self.slaves = []
        self.STAT = {}
        self.jobs = []
        self._timer = QTimer()
        self._timer.setInterval(15000)
        self._timer.timeout.connect(self.check_jobs)

    def start_maya_test(self):
        self.slaves = self.get_slaves()
        print '%s slaves found' % len(self.slaves)
        # docs.thinkboxsoftware.com/products/deadline/8.0/1_User Manual/manual/manual-submission.html
        print 'Start render...'
        prev = None
        for slave in self.slaves:
            outdir = os.path.join(os.path.dirname(self.scene_file), 'render', slave).replace('\\', '/')
            JobInfo = {
                "Name": self.job_prefix + slave,
                "UserName": getpass.getuser(),
                "Frames": "1-%s" % self.frame_count,
                "Plugin": "MayaBatch",
                "Priority": "100",
                "Whitelist": slave,
                "BatchName": 'Benchmark Test'
            }
            PluginInfo = {
                "Version": '2016',
                "SceneFile": self.scene_file,
                "ProjectPath": os.path.dirname(self.scene_file),
                "OutputFilePath": outdir
            }
            if self.chain and prev:
                JobInfo["JobDependencies"] = prev
            newJob = con.Jobs.SubmitJob(JobInfo, PluginInfo)
            prev = newJob['_id']
            self.jobs.append(newJob['_id'])
        self.start_timer()

    def start_houdini_test(self):
        print 'Not implement yet'

    def get_slaves(self):
        slaves_info = con.Slaves.GetSlaveInfos()
        names = [x['Name'] for x in slaves_info]
        if not names:
            print 'Slaves not found'
            self.exit()
        return names

    def start_timer(self):
        self._timer.start()

    def check_jobs(self):
        for jobid in self.jobs[:]:
            job = con.Jobs.GetJob(jobid)
            if not job:
                self.jobs.remove(jobid)
                continue
            if job['Stat'] == 3:
                self.save_job_result(job)
                self.jobs.remove(jobid)
                con.Jobs.DeleteJob(jobid)
        if not self.jobs:
            self.save_result()

    def save_job_result(self, job):
        tasks = con.Tasks.GetJobTasks(job['_id'])['Tasks']
        rendertimes = []
        for task in tasks:
            start = parse(str(task['StartRen']), ignoretz=True)
            end = parse(str(task['Comp']), ignoretz=True)
            rendertimes.append((end - start).total_seconds())
        average = round(sum(sorted(rendertimes)[1:-1]) / (len(rendertimes) - 2), 2)

        slave = job['Props']['ListedSlaves'][0]
        self.STAT[slave] = average
        print '%s \t\t: ~%s sec' % (slave, average)
        self.updateSignal.emit()

    def save_result(self):
        print 'Save result to', self.db_file
        factors = self.compute_factor(self.STAT)
        if factors:
            json.dump(factors, open(self.db_file, 'w'), indent=2)
        else:
            print 'Statistic is empty'

        self.completeSignal.emit()
        os.startfile(self.db_file)
        self.exit()

    def compute_factor(self, info):
        if not info:
            return {}
        if self.main_slave:
            base_time = info[self.main_slave]
        else:
            base_time = min(info.values())
        return {k: round(1.0/(v/base_time), 5) for k, v in info.items()}

    def exit(self):
        sys.exit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-rep", type=str, help="Deadline Repository")
    parser.add_argument("-scn", type=str, help="Maya Scene")
    parser.add_argument("-out", type=str, help="Output JSON file")
    parser.add_argument("-hst", type=str, help="Deadline WebService host")
    parser.add_argument("-prt", type=int, default=8082, help="Deadline WebService port")
    parser.add_argument("-slv", type=str, help="Main slave to compute factor")
    parser.add_argument("-frm", type=int, default=1, help="Frame count per job")
    parser.add_argument("-chn", type=int, default=0, help="Render jobs in queue chain")
    args = parser.parse_args()

    HOST = 'localhost'
    PORT = 8082

    if args.rep:
        sys.path.append(args.rep)
    if not args.scn:
        print 'Maya scene not set'
        sys.exit()
    if not args.rep:
        print 'Deadline Repository not set'
        sys.exit()
    if not args.out:
        print 'Output file not set'
        sys.exit()
    if not args.hst:
        print 'Deadline WebService host not set'
    else:
        HOST = args.hst
        sys.exit()
    if args.prt:
        PORT = args.prt
    if not args.slv:
        print 'Main slave not set and will be selected automatically'

    con = Connect.DeadlineCon(HOST, PORT)
    app = QCoreApplication([])
    o = DeadlineBenchmark(args.scn,
                          args.frm,
                          main_slave=args.slv,
                          frame_count=args.frm,
                          chain=args.chn)
    o.start_maya_test()
    app.exec_()


# Slave stats
# 1 - render
# 2 - ready
# 3 - offline
