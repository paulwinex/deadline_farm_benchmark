# Deadline Farm Benchmark

Simply benchmark for deadline render manager

Call command "mbf -h" for help

## Flags

    - rep    Deadline Repository path
    - scn    Maya Scene
    - out    Output JSON file
    - hst    Deadline WebService host
    - prt    Deadline WebService port (default 8082)
    - slv    Main slave to compute factor
    - frm    Frame count per job (minimum 5)
    - chn    Render jobs in queue chain

## Example

```
cd farm_benchmark
bmf -rep c:\programms\deadline8 -hst localhost -scn c:\scenes\test_scene.ma -out c:\hosts.json -slv pc001 -frm 20
```
